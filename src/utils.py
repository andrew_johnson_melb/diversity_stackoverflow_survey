import pandas as pd


def read_dataset(path, col_subset, dropna=True):
    """
    Read in the csv data stored at 'path' and select the
    subset of columns defined by 'col_subset'.  By default 'any'
    row with missing data is dropped

    # Parameters
      path, str, path to file
      col_subset, list, list of column names to keep
      dropna, bool, whether or not to drop na's

    # Return
      df, pandas dataframe

    """
    # Read data
    df = pd.read_csv(path)
    # Select the subset of cols we want to look at.
    df = df.loc[:, col_subset]
    # Ignore any missing values.
    if dropna:
        df = df.dropna(how='any')
    return df


def check_missing_pct(df):
    """
    Print the pct of missing values in dataframe 'df'

    # Parameters
      df, pandas dataframe

    # Returns
      pandas dataframe, missing pct

    """
    missing_pct_df = 100. * df.isnull().sum(axis=0).astype(float) / df.shape[0]
    return missing_pct_df


def process_str_feature(df, string_feature, iloc_max):
    """
    Convert the raw string feature feature and one-hot encode it.
    Note, we only consider the first race mentioned (as multiple races can be mentioned)

    # Parameters
        df, dataframe, survey data with column 'string_feature'

    # Returns
        df, dataframe with encoded feature
        one_hot_features, list of encoded features names

    """
    # Split the string in column by ;
    df_split = df[string_feature].str.split(';', expand=True)

    # let's only consider the first race mentioned in the text.
    df_one_hot = pd.get_dummies(df_split).iloc[:, 0:iloc_max]

    # keep track of the colum names
    one_hot_features = df_one_hot.columns.values

    # Remove RaceEthnicity col
    df.drop([string_feature], axis=1, inplace=True)

    # Add one-hot-encoded data into main df
    df_one_hot = pd.concat([df_one_hot, df], axis=1)

    return df_one_hot, one_hot_features


def create_pct_dist_df(df, features_list, feature_names):
    """
    From the provided data 'df' extract the features defined in 'features_list'.
    Decompose this data into a dataframe of percentage of occurance.

    Note, the features in 'features_list' need to be one-hot encoded.

    # Parameters
      df, input pandas dataframe,
      features_list, list, list of feature names to analysise
      feature_names, str, label for feature type

    # Returns
     pct_df, pandas dataframe with, columns = ['Percentage', 'feature_names']

    """
    # Create dataframe with feature vs percentage.
    total_count = df.loc[:, features_list].sum(axis=0).sum()
    pct_df = (100. * (df.loc[:, features_list].sum(axis=0) / total_count))
    pct_df = pd.DataFrame(pct_df).reset_index().rename(
        columns={0: 'Percentage', 'index': feature_names})

    return pct_df
