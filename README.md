# Diversity in Tech: Miles To Go

The aim of this project is simply to reinforce the well known fact
that there is a significant lack of diversity in the tech industry.

This project presents a very simple analysis of the current 
levels of diversity in the tech industry. We measure the distributions of 
Ethnicity, Gender, and education of Parents using a recent tech survey. 
We then compare these measurements to statistics from the global population. This allows us
to determine the degrees of misrepresentation for each diversity measure.

# Data

The statistics presented here are measured from the 2018 stackover flow survey.
This survey contains around 100,000 responses from a range of people.
see [survey data](https://insights.stackoverflow.com/survey/2018)

To reproduce this analysis you will need to download and upzip the 2018 survey data.
Make sure to update the ```BASE_DIR``` varible in the notebook to the location where you 
unzipped the data.

To measure the lack of representation for a particle group we need global measurements. 
For this purpose, we will use the 2010 U.S Census [https://www.census.gov/prod/cen2010/briefs/c2010br-02.pdf]. 
Note, the survey is international, however, it's a reasonable assumption that a 
significant proportion of respondents are from America.

# Results

As the title of this analysis alludes to, the current state-of-affairs suggest there are miles to go.

### Results: Ethnicity

What does the distribution of Ethnicity look like in the tech industry?
Our key observations are that the ethnicities Hispanic and African descent (or "black") are significantly underrepresented.

From the U.S census data, Hispanics make up 16% of the population. While within the Tech industry 6.8% of the respondents 
considered themselves Hispanic. Similarly, "black" or African descent represent 12% of the America population, we measure 
the representation as 2.3% in the tech industry.

### Results: Gender

We don't need to consult the census data to understand what 
the distributions of genders should look like. From the survey data we find 
that 90% of the respondeds were males and 6% were female. 
This showcases an amazing lack of diversity in gender.

The Transgender population is also underrepresenationed -- to a smaller degree. We measure 
0.25% in tech compare to 0.65% for the global population 
(see, [reference article] (https://williamsinstitute.law.ucla.edu/research/census-lgbt-demographics-studies/how-many-people-are-lesbian-gay-bisexual-and-transgender/))

### Results: Socioeconomic Status, Education of Parents

As highlighted above, we use parents education as a proxy for measuring the 
socioeconomic status of the individual: wealthy parents may provide more 
opportunities for their children and this could cause a skew in the 
representations in the tech industry. For reference data, we will 
use the statistics on educational attainment from the U.S. Census Bureau.

A number of observations could be made here. From the global data, 
individuals with Master's and/or doctorate and/or professional 
degree make up 12.83% of the population. This value is 30% in the 
tech industry. Therefore, individuals with well-educated parents 
are significantly overrepresented in tech.

# Running the notebook

To get started running this notebook first create a new conda enviroment
and then enter the enviroment.

```bash
$ conda env create diversity_stats
$ conda activate diversity_stats
```

Now install the dependencies

```bash
$ pip install matplotlib, pandas, seaborn, jupyter
```

Once the packages are installed start a jupyter server and open the 
notebook

```bash
$ jupyter notebook
```

# Files

```./stack_overflow_analysis.ipynb```  Jupyter notebook which presents analysis of diversity measures

```./src.utils.py``` stores utility functions that are used throughout the notebook

```./plots/``` directory to store plots created within the notebook


